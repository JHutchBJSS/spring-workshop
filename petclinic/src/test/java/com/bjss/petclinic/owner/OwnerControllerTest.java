package com.bjss.petclinic.owner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class OwnerControllerTest {
    @Mock
    OwnerRepository repository;

    @Test
    public void getAll(){
        //given

        //when

        //then
        assertThat(new OwnerController(repository).getAll(), is(nullValue()));
    }

    @Test
    public void getAllShouldReturnNotEmptyList() {
        Mockito.when(repository.findAll()).thenReturn(Collections.singletonList(new Owner(12,"Lucy")));

        Iterable<Owner> aas = new OwnerController(repository).getAll();
        assertNotNull(aas);
    }

}