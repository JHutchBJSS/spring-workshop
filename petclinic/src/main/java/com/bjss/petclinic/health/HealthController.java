package com.bjss.petclinic.Health;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/health")
final class HealthController {
    @GetMapping
    public String getHealt() {
        return "good";
    }
}
