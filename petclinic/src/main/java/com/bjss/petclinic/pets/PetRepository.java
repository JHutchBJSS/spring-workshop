package com.bjss.petclinic.pets;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface PetRepository extends CrudRepository<Pet, Long> {

}