package com.bjss.petclinic.pets;

import java.util.*;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/pets")
final class PetController {
    private final PetRepository repository;
    PetController(PetRepository repository)
    {
      this.repository = repository;
    }

    @GetMapping
    public Iterable<Pet> getAll() {
       return repository.findAll();
    }

    // Allows a get request to be made with a user defined argument on id
    // localhost:8080/pets/[id]
    @GetMapping("/{id}")
    public List<Pet> getPetById(@PathVariable("id") Long id) {
       return Collections.singletonList(new Pet(id, "Spot"));
    }

    // Allows a new value to be inserted on name using a post request
    // Ensure  @Valid is included so a valid string is returned
    @PostMapping
    public Pet create(@RequestBody @Valid PetRequest req) {

       return new Pet(3L, req.getName());
    }
}
