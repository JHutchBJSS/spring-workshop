package com.bjss.petclinic.pets;

import javax.persistence.*;

@Entity
@Table(name= "pet")
public class Pet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    Pet() {

    }

    Pet(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId(){
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
