package com.bjss.petclinic.owner;

import javax.persistence.*;

@Entity

public class Owner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    Owner(){

    }

    Owner(long id, String name){
       this.id = id;
       this.name = name;
    }

    public long getid(){
        return id;
    }

    public String getName(){
        return name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}