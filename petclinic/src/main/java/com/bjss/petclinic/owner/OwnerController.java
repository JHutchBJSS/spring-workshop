package com.bjss.petclinic.owner;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
@RestController
@RequestMapping(path = "/owners")
public class OwnerController {
    private final OwnerRepository repository;

    OwnerController(OwnerRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public Iterable<Owner> getAll() {
        return repository.findAll();
    }

//    @GetMapping
//    public List<Owner> getById(){
//        return Collections.singletonList(new Owner( 1,"Toby"));
//    }

    @GetMapping(path = "/{id}")
    public Owner getByID(@PathVariable("id") Long id) {
        return new Owner(id,"David");
    }

    @PostMapping
    public Owner create(@Valid @RequestBody OwnerRequest req){
        return new Owner(124, req.getName());
    }
}
