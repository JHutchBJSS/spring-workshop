package com.bjss.petclinic.owner;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
interface OwnerRepository extends CrudRepository<Owner, Long>{
}

