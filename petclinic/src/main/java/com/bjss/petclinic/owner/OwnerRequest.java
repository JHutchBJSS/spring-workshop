package com.bjss.petclinic.owner;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

final class OwnerRequest {
    @NotNull
    @Size(min = 1)
    private  String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
