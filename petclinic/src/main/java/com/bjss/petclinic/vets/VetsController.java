package com.bjss.petclinic.vets;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/vets")
final class VetsController {
    private final VetsRepository repository;

    VetsController(VetsRepository repo){
        this.repository = repo;
    }

    @GetMapping
    public Iterable<Vet> getAll(){
        return repository.findAll();
    }

//    @GetMapping
//    public List<Vet> getAll() {
//        return Arrays.asList(new Vet(20, "Billy"), new Vet(10, "Seb"));
//    }

    @GetMapping(path = "/{id}")
    public Vet getByID(@PathVariable("id") Long id){
        return new Vet(id,  "Joe");
    }

    @PostMapping
    public Vet create(@Valid @RequestBody VetsRequest req){
        return new Vet(02, req.getName());
    }

}
