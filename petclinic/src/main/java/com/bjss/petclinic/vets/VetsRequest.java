package com.bjss.petclinic.vets;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VetsRequest {
    @Size(min = 1)
    @NotNull
    private String name;


    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
