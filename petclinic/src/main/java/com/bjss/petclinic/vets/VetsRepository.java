package com.bjss.petclinic.vets;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface VetsRepository extends CrudRepository<Vet, Long>{

}